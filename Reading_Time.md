---
layout: page
title: Reading time!
permalink: /Reading_Time/
---

Since 2015 with "Operación Princesa", to now I've read the next list of books:

* Collection Antonio Salas (over 5 books)
* Fight Club - Chuck Palahniuk
* The Art of War - Sun Tzu
* The Anticrist - Friedrich Nietzsche
* In the beginning was the command line - Neil Stephenson
* Spy's diary - David R. Vidal
* 1984 - George Orwell
* Memories of a mangy lover - Groucho Marx (his autobiography)
* Hacker ethics and the spirit of the information age - Pekka Himanen
* The power of now - Eckhart Tolle

  
  

### “A mind needs books like a sword needs a whetstone, if it is to keep its edge. That is why I read so much.” (Tyrion Lannister - A game of Thrones)