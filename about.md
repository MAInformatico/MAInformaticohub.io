---
layout: page
title: About
permalink: /about/
---
I'm Miguel Angel Gutiérrez, in this blog I'll upload some posts about Technology, Computer Sciences or other interested subjects.
I'm learning English and French, so I'll try to post in the both languages and obviously in Spanish, my mother tongue.

#### More Information

As well as my professional career I'm passionate of sport, Heavy Metal, French Hip Hop and reading.
